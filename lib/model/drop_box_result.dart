import 'package:app_photo_test/model/drop_box_item.dart';

class DropBoxResult {
  DropBoxItem data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  DropBoxResult(this.data, this.isSuccess, this.code, this.msg);

  factory DropBoxResult.fromJson(Map<String, dynamic> json) {
    return DropBoxResult(
        DropBoxItem.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']);
  }
}
