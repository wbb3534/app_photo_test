class DropBoxItem {
  String realPath;
  String fileId;
  String fileName;
  String fileSharedLink;

  DropBoxItem(this.realPath, this.fileId, this.fileName, this.fileSharedLink);

  factory DropBoxItem.fromJson(Map<String, dynamic> json) {
    return DropBoxItem(
      json['realPath'],
      json['fileId'],
      json['fileName'],
      json['fileSharedLink'],
    );
  }
}
