import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:image_picker/image_picker.dart';

import 'repository/file_upload_repository.dart';

class PagePhoto extends StatefulWidget {
  const PagePhoto({Key? key}) : super(key: key);

  @override
  State<PagePhoto> createState() => _PagePhotoState();
}

class _PagePhotoState extends State<PagePhoto> {
  final picker = ImagePicker();
  File? _resultFile;
  int _maxWidth = 1000;
  int _maxHeight = 1000;
  int _imgQuality = 80;

  Future<void> _uploadPhoto() async {
    await FileUploadRepository()
        .uploadPhoto(_resultFile!)
        .then((res) {
      if (res.isSuccess) {
        print('이미지 업로드 성공');
      } else {
        print('이미지 업로드 실패');
      }
    }).catchError((err) {
      print(err);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TextButton(
              onPressed: () async {
                _getImage();
              },
              child: Text('사진선택')
          ),
          TextButton(
              onPressed: () {
                _uploadPhoto();
              },
              child: Text('업로드')
          ),
        ],
      ),
    );
  }

  Future _getImage() async {
    await picker.getImage(
      source: ImageSource.gallery,
    ).then((file) {
      _resizeImage(File(file!.path)).then((resultFile) {
        setState(() {
          _resultFile = resultFile;
        });
      });
    });
  }

  Future<File> _resizeImage(File file) async {


    ImageProperties properties = await FlutterNativeImage.getImageProperties(file.path);

    if (properties.width! > properties.height!) {
      // 가로가 더 긴 이미지
      File compressedFile = await FlutterNativeImage.compressImage(
        file.path,
        quality: _imgQuality,
        targetWidth: (properties.width! * _maxWidth / properties.height!).round(),
        targetHeight: _maxHeight,
      );

      File croppedFile = await FlutterNativeImage.cropImage(
        compressedFile.path,
        0,
        0,
        _maxWidth,
        _maxHeight,
      );

      return croppedFile;
    } else {
      // 세로가 더 긴 이미지
      File compressedFile = await FlutterNativeImage.compressImage(
        file.path,
        quality: _imgQuality,
        targetWidth: _maxWidth,
        targetHeight:
        (properties.height! * _maxHeight / properties.width!).round(),
      );

      File croppedFile = await FlutterNativeImage.cropImage(
        compressedFile.path,
        0,
        0,
        _maxWidth,
        _maxHeight,
      );

      return croppedFile;
    }
  }


}
