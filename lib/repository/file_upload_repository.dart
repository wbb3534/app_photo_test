import 'dart:io';

import 'package:app_photo_test/model/drop_box_result.dart';
import 'package:dio/dio.dart';

class FileUploadRepository {
  Future<DropBoxResult> uploadPhoto(File file) async {
    const String baseUrl = 'http://192.168.0.21:8090/v1/file/work-history';

    Dio dio = Dio();

    FormData formData = new FormData.fromMap({
      "file": await MultipartFile.fromFile(file.path),
    });

    final response = await dio.post(
        baseUrl,
        data: formData,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return DropBoxResult.fromJson(response.data);
  }
}

